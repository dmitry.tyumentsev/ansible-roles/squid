Squid role
=========

Installs squid proxy server on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.squid
  vars:
    squid_username: "{{ lookup('community.general.random_string', length=6) }}"
    squid_password: "{{ lookup('community.general.random_string', length=12) }}"
```
